# Pokemon Trainer Database

The [PokeApp](https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon) is a sample web
application designed to teach web development concepts at
the [Data & Information module](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/study-programme/#modules-technical-computer-science)
of the [Technical Computer Science](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/)
program of the [University of Twente](https://utwente.nl/en).

The contents of this repository are licensed
under [Creative Commons Attribution 4.0 International (CC-BY-4.0)](./LICENSE). Viewers are welcome to reuse its contents
and to submit their own contributions.

![](./design/models/Class%20Diagram.png)

## External resources

The following datasets and resources have been used in the development this project.

- [Pokemon Image Dataset](https://www.kaggle.com/datasets/vishalsubbiah/pokemon-images-and-types): dataset containing
  images for Pokemon types and and evolution chains.
- [The Complete Pokemon Dataset](https://www.kaggle.com/datasets/rounakbanik/pokemon): dataset containing detailed
  information about Pokemon types, stats, and more.
- [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page)
- [Pokémon Knowledge Graph](https://pokemonkg.org/)
- [PokéAPI](https://pokeapi.co/)

# Contributors

- Dr. Claudenir M. Fonseca
    - GitHub: [@claudenirmf](https://github.com/claudenirmf)
    - LinkedIn: [@claudenir-fonseca](https://www.linkedin.com/in/claudenir-fonseca/)
    - E-mail: [c.moraisfonseca@utwente.nl](c.moraisfonseca@utwente.nl)

Additional contributions are welcome in the forms of issues and merge requests.
